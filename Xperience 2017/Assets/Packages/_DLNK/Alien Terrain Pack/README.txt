Alien Terrain Pack TIP:

Dear customer, I hope you like the contents and  fit your project, but as Unity5 is different from other versions I'd give you some tips if your scene doesn't look the same as you expect from the video & screens:

- Enlighten requires to have "continuous baking" enabled if you want the baking to work in playmode.

- If the demo-character looks darker than it should remember to uncheck and check again the "continuous baking" option in the lighting panel.

That happens everytime you play the scene. All light probes become black and you need to do that "reset" so they look as they are.

- The golem character is not broken. It looks like it has no albedo texture because it's a demo-version of the charater only for test. The complete character version is available here: https://www.assetstore.unity3d.com/en/#!/content/13631

- If you find any troubles with the pack you can check for support here:
http://www.dlnkworks.com/support/

Thanks for your purchase!