﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchMagic : InputManager {
	public GameObject HandMagicVfx;
	public GameObject MagicShotVfx;
	bool HandTriggered;

	// Attach this script on the Right Hand

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if (PressHandTrigger()) {
			HandMagicVfx.SetActive (true);
			HandTriggered = true;
		}

		if (ReleaseHandTrigger() && HandTriggered) {
			MagicShotVfx.SetActive (true);
			HandMagicVfx.SetActive (false);
			HandTriggered = false;
		}

	}
}
