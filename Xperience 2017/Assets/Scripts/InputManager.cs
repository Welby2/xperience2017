﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

public class InputManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Pressing the Hand main Trigger
	public bool PressHandTrigger(){

		// Check for Oculus
		if (VRSettings.loadedDeviceName == "Oculus") {
			
			if (OVRInput.Get (OVRInput.Axis1D.PrimaryHandTrigger) > 0.7f)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	//Releasing the Hand main Trigger
	public bool ReleaseHandTrigger(){
		
		// Check for Oculus
		if (VRSettings.loadedDeviceName == "Oculus") {
			
			if (OVRInput.Get (OVRInput.Axis1D.PrimaryHandTrigger) < 0.2f)
				return true;
			else
				return false;
		} 
			
		else
			return false;

	}

	//Pressed down the A button
	public bool GetDownAButton(){

		// Check for Oculus
		if (VRSettings.loadedDeviceName == "Oculus") {

			if (OVRInput.GetDown (OVRInput.RawButton.A))
				return true;
			else
				return false;
		} 

		else
			return false;

	}


	//Pressed down the A button
	public bool GetDownBButton(){

		// Check for Oculus
		if (VRSettings.loadedDeviceName == "Oculus") {

			if (OVRInput.GetDown (OVRInput.RawButton.B))
				return true;
			else
				return false;
		} 

		else
			return false;

	}

}
